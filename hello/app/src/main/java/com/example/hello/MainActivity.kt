package com.example.hello

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import android.widget.ToggleButton

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val helloTextView = findViewById<TextView>(R.id.textView2)

        helloTextView.text = "Hello!"
        helloTextView.textSize = 42F

        val button: ToggleButton = findViewById<ToggleButton>(R.id.button1)
    }
}
