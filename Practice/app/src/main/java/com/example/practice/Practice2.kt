package com.example.practice

import android.os.Bundle
import android.app.Activity
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.content.Intent

import kotlinx.android.synthetic.main.activity_practice2.*

class Practice2 : AppCompatActivity() {
    companion object {
        val KEY_MILES = "message"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practice2)
        setSupportActionBar(toolbar)
        val key = ""
        val dataFromMainActivity:String = intent.getStringExtra(KEY_MILES)

        val returnButton = findViewById<Button>(R.id.return_button)
        val inMessage: TextView = findViewById(R.id.in_message)
        inMessage.text = dataFromMainActivity

        returnButton.setOnClickListener {
            val messageString: String = inMessage.text.toString()
            val returnIntent: Intent = Intent()
            returnIntent.putExtra(KEY_MILES, messageString)
            setResult(Activity.RESULT_OK, returnIntent)
            finish()
        }

    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        //Log.d("BSU", requestCode.toString())
        //Log.d("BSU", resultCode.toString())
        if (resultCode == Activity.RESULT_OK) {
            //Log.d("BSU", "This is the result from the second activity.")

            if (data != null) {
                val newMileage = data.getStringExtra(MainActivity.KEY_MILES1)

                if (newMileage != null) {
                    val inMessage: TextView = findViewById(R.id.in_message)
                    inMessage.text = newMileage
                }
            }
        }
    }
}
