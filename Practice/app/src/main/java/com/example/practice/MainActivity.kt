package com.example.practice

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.content.Intent
import android.app.Activity
import org.w3c.dom.Text


class MainActivity : AppCompatActivity() {
    companion object {
        val KEY_MILES1 = "message"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val sendButton: Button = findViewById(R.id.send_button)
        val messageText: EditText = findViewById(R.id.editText)



        sendButton.setOnClickListener {
            val intent = Intent(this, Practice2::class.java)
            val message : String = messageText.text.toString()
            intent.putExtra(Practice2.KEY_MILES,message)
            startActivityForResult(intent, 42)
            setResult(Activity.RESULT_OK, intent)
//            finish()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        //Log.d("BSU", requestCode.toString())
        //Log.d("BSU", resultCode.toString())
        if (resultCode == Activity.RESULT_OK) {
            //Log.d("BSU", "This is the result from the second activity.")

            if (data != null) {
                val newMileage = data.getStringExtra(Practice2.KEY_MILES)

                if (newMileage != null) {
                    val eText: TextView = findViewById(R.id.textView)
                    eText.text = newMileage
                }
            }
        }
    }
}


