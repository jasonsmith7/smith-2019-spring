package com.example.practice3

import android.app.Application

class myApplication: Application() {
   private var globalInitCount = 0

    public fun incVar(){
        globalInitCount++
    }

    public fun getVar(): Int{
        return globalInitCount
    }
}
