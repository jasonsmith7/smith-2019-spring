package com.example.practice3

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import io.objectbox.Box

class MainActivity : AppCompatActivity() {
    lateinit var toolbar: ActionBar
    lateinit var addButton: Button
    lateinit var songsFragment: ListFragment

    //instance variables for objectbox and transactions
    //lateinit var favBox: Box<Favorite>
    public var transaction: FragmentTransaction? = null

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_add -> {
               // title = "Save"
                    return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_remove -> {
                //title = "Remove"
                val intent = Intent(this, Main2Activity::class.java)
                intent.putExtra("count", 1)
                startActivityForResult(intent, 42)
                setResult(Activity.RESULT_OK, intent)

            }
            R.id.navigation_list -> {
                //title = "List"
                val songsFragment = ListFragment()
                openFragment(songsFragment)
               // return@OnNavigationItemSelectedListener true
            }

        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //title="Song Saver"
        toolbar = supportActionBar!!
        val bottomNavigation: BottomNavigationView = findViewById(R.id.navigationView)
        bottomNavigation.selectedItemId = R.id.navigation_add
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)

        val songFragment = SongAdd.newInstance()
        openFragment(songFragment)

        //init box
        //ObjectBox.init(this)





        var songName = findViewById<EditText>(R.id.editSong)
        var artistName = findViewById<EditText>(R.id.editArtist)

        addButton = findViewById(R.id.SaveButton)

        val favBox = ObjectBox.boxStore.boxFor(Favorite::class.java)


        addButton.setOnClickListener {

            //get data from user and create favorite object to be stored in box
            val temp1: String = songName.text.toString()
            val temp2: String = artistName.text.toString()
            var fave = Favorite()
            fave.song = temp1
            fave.artist = temp2
            val songFragment = SongAdd.newInstance()
            openFragment(songFragment)

            //line to comment out if I want to clear everything from my box
            // favBox.removeAll()

            //put the favorite object in the box
            favBox.put(fave)

            //clear the form for the next entry
            songName.text.clear()
            artistName.text.clear()

        }
    }
//    @Override
//    override fun onBackPressed() {
//
//
//            super.onBackPressed()
//            //additional code
//
//
//    }



    private fun openFragment(fragment: Fragment) {
        transaction = supportFragmentManager.beginTransaction()
       // val currFragment: Fragment? = getVisibleFragment()
        transaction!!.setCustomAnimations(R.anim.slide_up, R.anim.slide_down)
        transaction!!.replace(R.id.container, fragment)
        transaction!!.addToBackStack("list")
        transaction!!.show(fragment)
        transaction!!.commit()
    }
    private fun openSongFragment(fragment: Fragment) {
        transaction = supportFragmentManager.beginTransaction()
        // val currFragment: Fragment? = getVisibleFragment()
//        transaction!!.setCustomAnimations(R.anim.slide_up, R.anim.slide_down)
        transaction!!.replace(R.id.container, fragment)
        transaction!!.addToBackStack("list")
        transaction!!.show(fragment)
        transaction!!.commit()
    }



    fun OnListFragmentInteractionListener() {
        // TODO: Update argument type and name
        fun onListFragmentInteraction(item: Favorite?){}
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        //Log.d("BSU", requestCode.toString())
        //Log.d("BSU", resultCode.toString())
        if (resultCode == Activity.RESULT_OK) {
            //Log.d("BSU", "This is the result from the second activity.")

            if (data != null) {
               // super.onCreate(savedInstanceState)
                setContentView(R.layout.activity_main)
                val bottomNavigation: BottomNavigationView = findViewById(R.id.navigationView)
                bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
                val songFragment = SongAdd.newInstance()
                openFragment(songFragment)

                var songName = findViewById<EditText>(R.id.editSong)
                var artistName = findViewById<EditText>(R.id.editArtist)
                //var userName = findViewById<EditText>(R.id.editUser)
                //val songResult = findViewById<TextView>(R.id.songResult)
                //val artistResult = findViewById<TextView>(R.id.artistResult)
                addButton = findViewById(R.id.SaveButton)

                val favBox = ObjectBox.boxStore.boxFor(Favorite::class.java)
                // val todosBox = ObjectBox.boxStore.boxFor(ToDoItem::class.java)

                addButton.setOnClickListener {

                    //add form data to box
                    val temp1: String = songName.text.toString()
                    val temp2: String = artistName.text.toString()
                    // val temp3: String = userName.text.toString()
                    var fave = Favorite()
                    fave.song = temp1
                    fave.artist = temp2
                    val songFragment = SongAdd.newInstance()
                    openSongFragment(songFragment)

                    // favBox.removeAll()
                    favBox.put(fave)

                    songName.text.clear()
                    artistName.text.clear()
                }
            }
        }
    }


}
