package com.example.practice3


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.objectbox.Box
import io.objectbox.kotlin.query
import io.objectbox.query.QueryBuilder
import android.support.v4.app.FragmentTransaction
import android.widget.*


class ListFragment : Fragment() {
    lateinit var favBox: Box<Favorite>
    //private var transaction: FragmentTransaction? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_list, container, false)

        favBox = ObjectBox.boxStore.boxFor(Favorite::class.java)
        val favs = favBox.all
        var songItems = arrayOfNulls<String>(favs!!.size)
        var artistItems = arrayOfNulls<String>(favs.size)


        val query = favBox.query {
            order(Favorite_.id, QueryBuilder.DESCENDING)
        }
        val results = query.find()
//        for (i in favs.indices){
//            songItems[i] = favs[i].song
//            artistItems[i] = favs[i].artist
//        }
        var j = 0
        for (i in results){
            if(i.song.toString().length > 0) {
                var temp = i.song.toString()
                temp += " by " + i.artist.toString()

                songItems[j] = temp
                //artistItems[j] = i.artist
                j++
            }
        }
        var songs = view.findViewById<ListView>(R.id.songList)
        //var artists = view.findViewById<ListView>(R.id.artistList)

        val ad = ArrayAdapter(context, R.layout.list_item, songItems)
//        val ad1 = ArrayAdapter(context, R.layout.list_item, artistItems)

        songs.adapter = ad
//        artists.adapter = ad1

//        var exit: Button = view.findViewById(R.id.exitButton)
//        exit.setOnClickListener {
//        MainActivity().transaction.ge
//
//    }

        var back: Button = view.findViewById(R.id.exitButton)
        back.setOnClickListener{
//            onBackPressed()
            activity?.onBackPressed()
        }

        return view
    }
//    @Override
//    override fun onBackPressed() {
//
//
//            MainActivity.onBackPressed()
//            //additional code
//
//
//    }

    companion object {
        fun newInstance(): ListFragment = ListFragment()
    }

}
