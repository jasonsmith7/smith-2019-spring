package com.example.practice3

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

import com.example.practice3.dummy.DummyContent
import com.example.practice3.dummy.DummyContent.DummyItem
import io.objectbox.Box
import io.objectbox.kotlin.boxFor
import io.objectbox.kotlin.query
import io.objectbox.query.QueryBuilder

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [SongFragment.OnListFragmentInteractionListener] interface.
 */
class SongFragment : Fragment() {

    // TODO: Customize parameters
    lateinit var favBox: Box<Favorite>
    private var columnCount = 3

    private var listener: OnListFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_song_list, container, false)
        //val view1 = inflater.inflate(R.layout.fragment_song, container, false)
        favBox = ObjectBox.boxStore.boxFor(Favorite::class.java)
//        val songs = favBox?.all
//        var listItems = arrayOfNulls<String>(songs!!.size)
//        for (i in songs!!.indices){
//            listItems[i] = songs[i].song
//        }

        val query = favBox.query {
            order(Favorite_.id, QueryBuilder.DESCENDING)
        }
        val results = query.find()
        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                adapter = MySongRecyclerViewAdapter(favBox!!.all, listener)
            }
        }

//        val butt: Button = view.findViewById(R.id.button1)
//        butt.setOnClickListener {
//            activity?.supportFragmentManager?.beginTransaction()?.remove(this)?.commit()
//        }
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        //if (context is OnListFragmentInteractionListener) {
            //listener = context
//        } else {
//            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
//        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson
     * [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onListFragmentInteraction(item: Favorite?)
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
            SongFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
}
