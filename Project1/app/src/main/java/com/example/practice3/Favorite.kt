package com.example.practice3

import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id

@Entity
data class Favorite (
    @Id
    var id: Long = 0,
    var song: String? = null,
    var artist: String? = null
    //var user: String? =null
)