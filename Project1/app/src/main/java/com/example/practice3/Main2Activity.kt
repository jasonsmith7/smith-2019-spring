package com.example.practice3

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import io.objectbox.Box

class Main2Activity : AppCompatActivity() {
    lateinit var toolbar: ActionBar
    lateinit var addButton: Button
    var ct: Int = 1

    //lateinit var songName: String
    //lateinit var artistName: String
    // private var favBox: Box<Favorite>? = ObjectBox.get()?.boxFor()
    lateinit var favBox: Box<Favorite>
    private var transaction: FragmentTransaction? = null

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_add -> {
                title = "Save"
//                val songsFragment = SongFragment.newInstance(3)
//                openFragment(songsFragment)
                val intent = Intent(this, MainActivity::class.java)
                setResult(Activity.RESULT_OK, intent)
                finish()

//                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_remove -> {
                title = "Remove"
               // val intent = Intent(this, Main2Activity::class.java)
               // this.startActivity(intent)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_list -> {
                title = "List"
                val songsFragment = ListFragment()
                openFragment(songsFragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        title="Song Saver"

        val bottomNavigation: BottomNavigationView = findViewById(R.id.navigationView)
        toolbar = supportActionBar!!
        bottomNavigation.selectedItemId =  R.id.navigation_add
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        val songFragment = SongAdd.newInstance()
        openSongFragment(songFragment)

        var songName = findViewById<EditText>(R.id.editSong)

        addButton = findViewById(R.id.SaveButton)

        favBox = ObjectBox.boxStore.boxFor(Favorite::class.java)

        addButton.setOnClickListener {

            //add form data to box
            val temp1: String = songName.text.toString()

            val favs = favBox?.all

            for (i in favs!!.indices){
               if (favs[i].song == temp1) {
                   favBox.remove(favs[i].id)
                   songName.text.clear()
               }

            }

            val songFragment = SongAdd.newInstance()
            openFragment(songFragment)


        }
    }



    private fun openFragment(fragment: Fragment) {
        transaction = supportFragmentManager.beginTransaction()
        // val currFragment: Fragment? = getVisibleFragment()
        transaction!!.setCustomAnimations(R.anim.slide_up, R.anim.slide_down)
        transaction!!.replace(R.id.container, fragment)
        transaction!!.addToBackStack(null)
        transaction!!.show(fragment)
        transaction!!.commit()
    }
    private fun openSongFragment(fragment: Fragment) {
        transaction = supportFragmentManager.beginTransaction()
        // val currFragment: Fragment? = getVisibleFragment()
//        transaction!!.setCustomAnimations(R.anim.slide_up, R.anim.slide_down)
        transaction!!.replace(R.id.container, fragment)
        transaction!!.addToBackStack("list")
        transaction!!.show(fragment)
        transaction!!.commit()
    }

    fun OnListFragmentInteractionListener() {
        // TODO: Update argument type and name
        fun onListFragmentInteraction(item: Favorite?){}
    }

    override fun onStop() {
        // call the superclass method first
        super.onStop()


    }

//    private fun getVisibleFragment(): Fragment? {
//        val fragmentManager = this@MainActivity.supportFragmentManager
//        val fragments = fragmentManager.fragments
//        if (fragments != null) {
//            for (fragment in fragments) {
//                if (fragment != null && fragment.isVisible)
//                    return fragment
//            }
//        }
//        return null
//    }
}
