package com.example.practice3


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import io.objectbox.Box
import io.objectbox.kotlin.query
import io.objectbox.query.QueryBuilder


class SongAdd : Fragment() {

    lateinit var favBox: Box<Favorite>
    //private var transaction: FragmentTransaction? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_song_add, container, false)

        favBox = ObjectBox.boxStore.boxFor(Favorite::class.java)
        val favs = favBox.all
        var songItems = arrayOfNulls<String>(favs!!.size)
        //var artistItems = arrayOfNulls<String>(favs.size)

        for (i in favs.indices){
            songItems[i] = favs[i].song
            //artistItems[i] = favs[i].artist
        }
        val query = favBox.query {
            order(Favorite_.id, QueryBuilder.DESCENDING)
        }
        val results = query.find()
        var j = 0
        for (i in results){
            if(i.song.toString().length > 0){
                songItems[j] = i.song
                j++}
        }

        var songs = view.findViewById<ListView>(R.id.songList)
        //var artists = view.findViewById<ListView>(R.id.artistList)

        val ad = ArrayAdapter(context, R.layout.list_item, songItems)
        //val ad1 = ArrayAdapter(context, R.layout.list_item, artistItems)

        songs.adapter = ad
        //artists.adapter = ad1

//        songs.onItemClickListener = AdapterView.OnItemClickListener(ad,view,0,42)->{
//
//
//
//        }


//        for(i in results.indices){
//        }

        return view
    }

    companion object {
        fun newInstance(): SongAdd = SongAdd()
    }
}