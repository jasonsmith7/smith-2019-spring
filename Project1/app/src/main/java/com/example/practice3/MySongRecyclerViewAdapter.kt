package com.example.practice3

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView


import com.example.practice3.SongFragment.OnListFragmentInteractionListener
import com.example.practice3.dummy.DummyContent.DummyItem

import kotlinx.android.synthetic.main.fragment_song.view.*

/**
 * [RecyclerView.Adapter] that can display a [DummyItem] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 * TODO: Replace the implementation with code for your data type.
 */
class MySongRecyclerViewAdapter(
    private val mValues: List<Favorite>,
    private val mListener: OnListFragmentInteractionListener?
) : RecyclerView.Adapter<MySongRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Favorite
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            mListener?.onListFragmentInteraction(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_song, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        //holder.mIdView.text = item.user.toString()
        holder.mContentView.text = item.song.toString()
        holder.mContentView2.text = item.artist.toString()

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mIdView: TextView = mView.item_number
        val mContentView: TextView = mView.content
        val mContentView2: TextView = mView.user

        override fun toString(): String {
            return super.toString() + " '" + mContentView.text + " '" + mContentView2.text + "'"
        }
    }
}
