package com.example.practice3
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id

@Entity
data class Song (
    @Id
    var id: Long = 0,
    var title: String? = null
)