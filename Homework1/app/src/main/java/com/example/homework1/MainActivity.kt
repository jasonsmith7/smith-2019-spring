package com.example.homework1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Switch
import android.widget.TextView
import org.w3c.dom.Text

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var catVal = "____________"
        var addVal = "____________"
        val bg: android.support.constraint.ConstraintLayout = findViewById(R.id.background)
        val input1: EditText = findViewById(R.id.input1)
        val input2: EditText = findViewById(R.id.input2)
        val catButton: Button = findViewById(R.id.button)
        val addButton: Button = findViewById(R.id.button2)
        val resButton: Button = findViewById(R.id.button3)
        val intro: TextView = findViewById(R.id.textView)
        val catTextView: TextView = findViewById(R.id.textview1)
        val addTextView: TextView = findViewById(R.id.textview2)
        val invert: Switch = findViewById(R.id.switch1)
        val addAlert1: TextView = findViewById(R.id.addAlert)
        val catAlert2: TextView = findViewById(R.id.catAlert)

        //Log.d("BSU", "hello: $(output.toString())")
        catButton.setOnClickListener {
            if(!input1.text.toString().isNullOrBlank() && !input2.text.toString().isNullOrBlank()) {
                val output = input1.text.toString() + input2.text.toString()
                if (output == catVal) {
                    catAlert2.visibility = View.VISIBLE
                } else {
                    catAlert2.visibility = View.INVISIBLE
                }
                catVal = output
                catTextView.text = output

            }

        }
        addButton.setOnClickListener {
            if(!input1.text.toString().isNullOrBlank() && !input2.text.toString().isNullOrBlank()) {
                val output1 = Integer.parseInt(input1.text.toString()) + Integer.parseInt(input2.text.toString())
                if(output1.toString() == addVal){
                    //addAlert1.animate().alpha(1.0f);
                    addAlert1.visibility = View.VISIBLE
                    //addAlert1.animate().alpha(0.0f);
                }
                else {

                    addAlert1.visibility = View.INVISIBLE
                }
                addVal = output1.toString()
                addTextView.text = output1.toString()
            }

        }

        resButton.setOnClickListener {
            catTextView.text = "____________"
            addTextView.text = "____________"
            input1.text.clear()
            input2.text.clear()

        }

        //catTextView.textSize = 42F
        invert.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked){
                invert.text = "Dark Mode"
                val color = getResources().getColor(R.color.black)
                val color2 = getResources().getColor(R.color.white)
                intro.setBackgroundColor(color)
                intro.setTextColor(color2)
                //invert.setBackgroundColor(color2)
                //invert.setTextColor(color)
                addAlert1.setTextColor(color2)
                catAlert2.setTextColor(color2)
                bg.setBackgroundColor(color)
                catTextView.setBackgroundColor(color)
                addTextView.setBackgroundColor(color)
                catTextView.setTextColor(color2)
                addTextView.setTextColor(color2)
            }
            else{
                invert.text = "Light Mode"

                val color = getResources().getColor(R.color.white)
                val color2 = getResources().getColor(R.color.black)
                intro.setBackgroundColor(color)
                intro.setTextColor(color2)
                //invert.setBackgroundColor(color2)
                //invert.setTextColor(color)
                addAlert1.setTextColor(color2)
                catAlert2.setTextColor(color2)
                bg.setBackgroundColor(color)
                catTextView.setBackgroundColor(color)
                addTextView.setBackgroundColor(color)
                catTextView.setTextColor(color2)
                addTextView.setTextColor(color2)
            }
        }
    }
}
